# Docker documents

## Basic concepts.

 - **images**: Images are similar to classes from OOP. The are readonly.
 - **containers**: Containers are similar to objects from OOP. They are instance of images, you can multiple containers from same image. A container only *lives* if a running proccess exists, if not process exists the containers dies.
 - **networks**: Docker provides three basic network types: *bridge*, *host* and *none*, but you can add more networks to create isolated networks from your containers.
 - **volumes**: Volumes provides data persistent to docker containers data.

## Basic commands.

### Get docker images.

To get docker images you must use *pull* command. The syntax it's quite simply.

`
$ docker pull <image-name>:[tag]  # docker pull hello-world:latest
`

### TODO COMMANDS

`
$ docker run <image-name>:[tag]
$ docker exec <image-name>:[tag]
`
## TODO Dockerfile
